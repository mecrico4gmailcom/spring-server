package com.example.demo;

import com.example.demo.models.Consultation;
import com.example.demo.models.Doctor;
import com.example.demo.models.Patient;
import com.example.demo.repositories.ConsultationRepository;
import com.example.demo.repositories.DoctorRepository;
import com.example.demo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.List;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	private final DoctorRepository doctorRepo;
	private final PatientRepository patientRepo;
	private final ConsultationRepository consultationRepo;
	@Autowired
	public DemoApplication( DoctorRepository doctorRepo, PatientRepository patientRepo, ConsultationRepository consultationRepo) {
		this.doctorRepo = doctorRepo;
		this.patientRepo = patientRepo;
		this.consultationRepo = consultationRepo;
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//doctorRepo.deleteAll();
		//patientRepo.deleteAll();
		//consultationRepo.deleteAll();
		if (doctorRepo.findAll().isEmpty()) {
			doctorRepo.saveAll(List.of(
					new Doctor("Jerimiah", "Olsen", "Allergy and Immunology", LocalDate.of(2020, 1, 8)),
					new Doctor("Edward", "Burke", "Anesthesiology", LocalDate.of(2020, 1, 8)),
					new Doctor("Theresa", "Gilmore", "Dermatology", LocalDate.of(2020, 1, 8)),
					new Doctor("Reginald", "Mathews", "Diagnostic radiology", LocalDate.of(2020, 1, 8)),
					new Doctor("Carolina", "Liu", "Emergency medicine", LocalDate.of(2020, 1, 8)),
					new Doctor("Valentina", "Reeves", "Family medicine", LocalDate.of(2020, 1, 8)),
					new Doctor("Renee", "Spears", "Internal medicine", LocalDate.of(2020, 1, 8)),
					new Doctor("Amelia", "Berry", "Medical genetics", LocalDate.of(2020, 1, 8)),
					new Doctor("Ayaan", "Wallace", "Allergy and Immunology", LocalDate.of(2020, 1, 8)),
					new Doctor("Bo", "Underwood", "Anesthesiology", LocalDate.of(2020, 1, 8)),
					new Doctor("Angelica", "Beck", "Dermatology", LocalDate.of(2020, 1, 8)),
					new Doctor("Cordell", "Gillespie", "Diagnostic radiology", LocalDate.of(2020, 1, 8))
			));
		}
	 	if(patientRepo.findAll().isEmpty()) {
			patientRepo.saveAll(List.of(
					new Patient("Autumn", "Hess", LocalDate.of(2020, 1, 8), "7520 SW. Broad Ave."),
					new Patient("Gisselle", "Cole", LocalDate.of(2020, 1, 8), "Georgetown, SC 29440"),
					new Patient("Anaya", "Doyle", LocalDate.of(2020, 1, 8), "9126 Greenrose St."),
					new Patient("Conrad", "Landry", LocalDate.of(2020, 1, 8), "Yonkers, NY 10701"),
					new Patient("Esmeralda", "Rios", LocalDate.of(2020, 1, 8), "70 Sunnyslope Street"),
					new Patient("Blake", "Armstrong", LocalDate.of(2020, 1, 8), "Selden, NY 11784"),
					new Patient("Brogan", "Hardin", LocalDate.of(2020, 1, 8), "70 Morris Lane"),
					new Patient("Oscar", "Larson", LocalDate.of(2020, 1, 8), "Hammonton, NJ 08037"),
					new Patient("Lola", "Meyer", LocalDate.of(2020, 1, 8), "8 Big Rock Cove Street"),
					new Patient("Kaydence", "Key", LocalDate.of(2020, 1, 8), "Staten Island, NY 10301"),
					new Patient("Lucas", "Wolfe", LocalDate.of(2020, 1, 8), "376 West Airport Court"),
					new Patient("Braelyn", "Oneal", LocalDate.of(2020, 1, 8), "Upper Marlboro, MD 20772")
			));
		}

		if(consultationRepo.findAll().isEmpty()) {
			List<Doctor> doctors = doctorRepo.findAll();
			List<Patient> patients = patientRepo.findAll();

			consultationRepo.saveAll(List.of(
				new Consultation( "Test consultation 1", "Abemaciclib Tablets, Abilify Maintena, Abiraterone Acetate Tablets, Abobotulinumtoxin A Injection", LocalDate.of(2021, 5, 22), doctors.get(0), patients.get(0)),
				new Consultation( "Test consultation 1", "Abemaciclib Tablets, Abilify Maintena, Abiraterone Acetate Tablets, Abobotulinumtoxin A Injection", LocalDate.of(2021, 5, 22), doctors.get(1), patients.get(1)),
				new Consultation( "Test consultation 1", "Abemaciclib Tablets, Abilify Maintena, Abiraterone Acetate Tablets, Abobotulinumtoxin A Injection", LocalDate.of(2021, 5, 22), doctors.get(3), patients.get(2)),
				new Consultation( "Test consultation 1", "Abemaciclib Tablets, Abilify Maintena, Abiraterone Acetate Tablets, Abobotulinumtoxin A Injection", LocalDate.of(2021, 5, 22), doctors.get(4), patients.get(3)),
				new Consultation( "Test consultation 1", "Abemaciclib Tablets, Abilify Maintena, Abiraterone Acetate Tablets, Abobotulinumtoxin A Injection", LocalDate.of(2021, 5, 22), doctors.get(8), patients.get(4)),
				new Consultation( "Test consultation 1", "Aceclofenac Tablet, Film Coated, Acetaminophen, Caffeine and Dihydrocodeine Bitartrate, Acetaminophen", LocalDate.of(2021, 5, 22), doctors.get(10), patients.get(5)),
				new Consultation( "Test consultation 1", "Aceclofenac Tablet, Film Coated, Acetaminophen, Caffeine and Dihydrocodeine Bitartrate, Acetaminophen", LocalDate.of(2021, 5, 22), doctors.get(2), patients.get(6)),
				new Consultation( "Test consultation 1", "Aceclofenac Tablet, Film Coated, Acetaminophen, Caffeine and Dihydrocodeine Bitartrate, Acetaminophen", LocalDate.of(2021, 5, 22), doctors.get(9), patients.get(7)),
				new Consultation( "Test consultation 1", "Aceclofenac Tablet, Film Coated, Acetaminophen, Caffeine and Dihydrocodeine Bitartrate, Acetaminophen", LocalDate.of(2021, 5, 22), doctors.get(8), patients.get(8)),
				new Consultation( "Test consultation 1", "Aceclofenac Tablet, Film Coated, Acetaminophen, Caffeine and Dihydrocodeine Bitartrate, Acetaminophen", LocalDate.of(2021, 5, 22), doctors.get(7), patients.get(9)),
				new Consultation( "Test consultation 1", "Aceclofenac Tablet, Film Coated, Acetaminophen, Caffeine and Dihydrocodeine Bitartrate, Acetaminophen", LocalDate.of(2021, 5, 22), doctors.get(9), patients.get(10)),
				new Consultation( "Test consultation 1", "Aceclofenac Tablet, Film Coated, Acetaminophen, Caffeine and Dihydrocodeine Bitartrate, Acetaminophen", LocalDate.of(2021, 5, 22), doctors.get(6), patients.get(1)),
				new Consultation( "Test consultation 1", "Acetylcholine Chloride Intraocular Solution, Acetylcysteine Effervescent Tablets for Oral Solution", LocalDate.of(2021, 5, 22), doctors.get(6), patients.get(5)),
				new Consultation( "Test consultation 1", "Acetylcholine Chloride Intraocular Solution, Acetylcysteine Effervescent Tablets for Oral Solution", LocalDate.of(2021, 5, 22), doctors.get(2), patients.get(9)),
				new Consultation( "Test consultation 1", "Acetylcholine Chloride Intraocular Solution, Acetylcysteine Effervescent Tablets for Oral Solution", LocalDate.of(2021, 5, 22), doctors.get(8), patients.get(7)),
				new Consultation( "Test consultation 1", "Acetylcholine Chloride Intraocular Solution, Acetylcysteine Effervescent Tablets for Oral Solution", LocalDate.of(2021, 5, 22), doctors.get(7), patients.get(5)),
				new Consultation( "Test consultation 1", "Acetylcholine Chloride Intraocular Solution, Acetylcysteine Effervescent Tablets for Oral Solution", LocalDate.of(2021, 5, 22), doctors.get(10), patients.get(3)),
				new Consultation( "Test consultation 1", "Acetylcholine Chloride Intraocular Solution, Acetylcysteine Effervescent Tablets for Oral Solution", LocalDate.of(2021, 5, 22), doctors.get(3), patients.get(6)),
				new Consultation( "Test consultation 1", "Acetylcholine Chloride Intraocular Solution, Acetylcysteine Effervescent Tablets for Oral Solution", LocalDate.of(2021, 5, 22), doctors.get(7), patients.get(5)),
				new Consultation( "Test consultation 1", "Acetylcholine Chloride Intraocular Solution, Acetylcysteine Effervescent Tablets for Oral Solution", LocalDate.of(2021, 5, 22), doctors.get(2), patients.get(4)),
				new Consultation( "Test consultation 1", "Acetylcholine Chloride Intraocular Solution, Acetylcysteine Effervescent Tablets for Oral Solution", LocalDate.of(2021, 5, 22), doctors.get(1), patients.get(8)),
				new Consultation( "Test consultation 1", "Acetylcholine Chloride Intraocular Solution, Acetylcysteine Effervescent Tablets for Oral Solution", LocalDate.of(2021, 5, 22), doctors.get(6), patients.get(5)),
				new Consultation( "Test consultation 1", "Acetic Acid, Acetohydroxamic Acid Tablets, Acetyl Sulfisoxazole Pediatric Suspension", LocalDate.of(2021, 5, 22), doctors.get(8), patients.get(10)),
				new Consultation( "Test consultation 1", "Acetic Acid, Acetohydroxamic Acid Tablets, Acetyl Sulfisoxazole Pediatric Suspension", LocalDate.of(2021, 5, 22), doctors.get(5), patients.get(8)),
				new Consultation( "Test consultation 1", "Acetic Acid, Acetohydroxamic Acid Tablets, Acetyl Sulfisoxazole Pediatric Suspension", LocalDate.of(2021, 5, 22), doctors.get(4), patients.get(5)),
				new Consultation( "Test consultation 1", "Acetic Acid, Acetohydroxamic Acid Tablets, Acetyl Sulfisoxazole Pediatric Suspension", LocalDate.of(2021, 5, 22), doctors.get(5), patients.get(2)),
				new Consultation( "Test consultation 1", "Acetic Acid, Acetohydroxamic Acid Tablets, Acetyl Sulfisoxazole Pediatric Suspension", LocalDate.of(2021, 5, 22), doctors.get(9), patients.get(6)),
				new Consultation( "Test consultation 1", "Acetic Acid, Acetohydroxamic Acid Tablets, Acetyl Sulfisoxazole Pediatric Suspension", LocalDate.of(2021, 5, 22), doctors.get(10), patients.get(4)),
				new Consultation( "Test consultation 1", "Acetic Acid, Acetohydroxamic Acid Tablets, Acetyl Sulfisoxazole Pediatric Suspension", LocalDate.of(2021, 5, 22), doctors.get(6), patients.get(8)),
				new Consultation( "Test consultation 1", "Acetic Acid, Acetohydroxamic Acid Tablets, Acetyl Sulfisoxazole Pediatric Suspension", LocalDate.of(2021, 5, 22), doctors.get(8), patients.get(0)),
				new Consultation( "Test consultation 1", "Acetylcysteine Injection, Acetylcysteine Solution, Aciphex Sprinkle", LocalDate.of(2021, 5, 22), doctors.get(1), patients.get(10)),
				new Consultation( "Test consultation 1", "Acetylcysteine Injection, Acetylcysteine Solution, Aciphex Sprinkle", LocalDate.of(2021, 5, 22), doctors.get(5), patients.get(3)),
				new Consultation( "Test consultation 1", "Acetylcysteine Injection, Acetylcysteine Solution, Aciphex Sprinkle", LocalDate.of(2021, 5, 22), doctors.get(9), patients.get(8)),
				new Consultation( "Test consultation 1", "Acetylcysteine Injection, Acetylcysteine Solution, Aciphex Sprinkle", LocalDate.of(2021, 5, 22), doctors.get(6), patients.get(7)),
				new Consultation( "Test consultation 1", "Acetylcysteine Injection, Acetylcysteine Solution, Aciphex Sprinkle", LocalDate.of(2021, 5, 22), doctors.get(3), patients.get(4)),
				new Consultation( "Test consultation 1", "Acetylcysteine Injection, Acetylcysteine Solution, Aciphex Sprinkle", LocalDate.of(2021, 5, 22), doctors.get(0), patients.get(1)),
				new Consultation( "Test consultation 1", "Acalabrutinib Capsules, Acamprosate Calcium, Acanya Gel, Acetic Acid", LocalDate.of(2021, 5, 22), doctors.get(5), patients.get(5)),
				new Consultation( "Test consultation 1", "Acalabrutinib Capsules, Acamprosate Calcium, Acanya Gel, Acetic Acid", LocalDate.of(2021, 5, 22), doctors.get(0), patients.get(6)),
				new Consultation( "Test consultation 1", "Acalabrutinib Capsules, Acamprosate Calcium, Acanya Gel, Acetic Acid", LocalDate.of(2021, 5, 22), doctors.get(3), patients.get(9)),
				new Consultation( "Test consultation 1", "Acalabrutinib Capsules, Acamprosate Calcium, Acanya Gel, Acetic Acid", LocalDate.of(2021, 5, 22), doctors.get(10), patients.get(8))
			));
		}

	}
}
