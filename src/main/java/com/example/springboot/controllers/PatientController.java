package com.example.demo.controllers;

import com.example.demo.models.Consultation;
import com.example.demo.models.Patient;
import com.example.demo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class PatientController {

    private final PatientRepository patientRepository;
    private final MongoTemplate mongoTemplate;

    @Autowired
    public PatientController(PatientRepository patientRepository, MongoTemplate mongoTemplate) {
        this.patientRepository = patientRepository;
        this.mongoTemplate = mongoTemplate;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/patients")
    public List<Patient> getPatients(){
        return patientRepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/patient")
    public Optional<Patient> getPatient(@RequestParam String id){
        return patientRepository.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/patients/search")
    public List<Patient> searchPatients(@RequestParam String term) {
        if (term.isEmpty()) {
            return List.of();
        }
        ExampleMatcher ignoringExampleMatcher = ExampleMatcher.matchingAny()
            .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
            .withMatcher("lastName", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
            .withIgnorePaths("birthday", "address");

        Example<Patient> example = Example.of(new Patient(term, term, null, null), ignoringExampleMatcher);
        return patientRepository.findAll(example);
    }

}
