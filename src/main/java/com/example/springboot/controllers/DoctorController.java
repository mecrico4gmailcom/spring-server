package com.example.demo.controllers;

import com.example.demo.models.Doctor;
import com.example.demo.models.Patient;
import com.example.demo.repositories.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class DoctorController {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorController(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/doctors")
    public List<Doctor> getDoctors(){
        return doctorRepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/doctor")
    public Optional<Doctor> getDoctor(@RequestParam String id){
        return doctorRepository.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/doctors/search")
    public List<Doctor> searchDoctors(@RequestParam String term) {
        if (term.isEmpty()) {
            return List.of();
        }
        ExampleMatcher ignoringExampleMatcher = ExampleMatcher.matchingAny()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
                .withMatcher("lastName", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
                .withMatcher("medicalSpecialty", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
                .withIgnorePaths("birthday", "address");

        Example<Doctor> example = Example.of(new Doctor(term, term, term, null), ignoringExampleMatcher);
        return doctorRepository.findAll(example);
    }

}
