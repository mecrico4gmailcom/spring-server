package com.example.demo.controllers;

import com.example.demo.models.Consultation;
import com.example.demo.models.Doctor;
import com.example.demo.models.Patient;
import com.example.demo.repositories.ConsultationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.List;

@RestController
public class ConsultationController {

    private final ConsultationRepository consultationRepository;
    private final MongoTemplate mongoTemplate;

    @Autowired
    public ConsultationController(ConsultationRepository consultationRepository, MongoTemplate mongoTemplate) {
        this.consultationRepository = consultationRepository;
        this.mongoTemplate = mongoTemplate;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/consultations")
    public List<Consultation> getConsultations() {
        return consultationRepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/consultations/doctor")
    public List<Consultation> getDoctorConsultations(@RequestParam String id) {
        Query query = new Query().addCriteria(Criteria.where("doctor").is(id));
        return mongoTemplate.find(query, Consultation.class);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/consultations/patient")
    public List<Consultation> getPatientConsultations(@RequestParam String id) {
        Query query = new Query().addCriteria(Criteria.where("patient").is(id));
        return mongoTemplate.find(query, Consultation.class);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(value = "/consultations/create",
        consumes = {MediaType.APPLICATION_JSON_VALUE},
        produces = {MediaType.APPLICATION_JSON_VALUE})
    public Consultation addConsultations(@RequestBody Consultation consultation, HttpServletResponse response) {
        System.out.println(consultation.toString());
        if(consultation.validateFields()){
            Consultation savedCon = consultationRepository.save(consultation);
            return savedCon;
        }
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        return null;
    }
}
