package com.example.demo.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDate;

@Document
public class Consultation {

    @Id
    private String id;
    @Field
    private String description;
    @Field
    private LocalDate appointmentDate;
    @Field
    private String prescription;
    @DBRef
    private Doctor doctor;
    @DBRef
    private Patient patient;

    public Consultation(){}

    public Consultation(String description, String prescription, LocalDate appointmentDate, Doctor doctor, Patient patient) {
        this.description = description;
        this.prescription = prescription;
        this.appointmentDate = appointmentDate;
        this.doctor = doctor;
        this.patient = patient;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(LocalDate appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public String toString() {
        return "Consultation{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                ", appointmentDate=" + appointmentDate +
                ", prescription='" + prescription + '\'' +
                ", doctor=" + doctor +
                ", patient=" + patient +
                '}';
    }

    public Boolean validateFields() {
        return description != null && !description.isEmpty() &&
                prescription != null && !prescription.isEmpty() &&
                appointmentDate != null &&
                doctor != null &&
                patient != null;
    }
}
